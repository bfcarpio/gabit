package gabit_test

import (
	"encoding/json"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "gitlab.com/bfcarpio/gabit"
)

var _ = Describe("Webhook structs", func() {
	It("will seriealize", func() {
		data := []byte(`{"enabled": true,"url": "http://some-webhook-url.com","label": "My Quest Webhook","type": "questActivity","options": {"questStarted": false,"questFinished": false,"questInvited": false}}`)
		var w Webhook
		err := json.Unmarshal(data, &w)
		Expect(err).ToNot(HaveOccurred())
		Expect(w.Enabled).To(Equal(true))
		Expect(w.Type).To(Equal(QuestActivity))
		Expect(w.URL).To(Equal("http://some-webhook-url.com"))
		Expect(w.Label).To(Equal("My Quest Webhook"))
		Expect(w.Options).To(Equal(map[string]interface{}{
			"questStarted":  false,
			"questFinished": false,
			"questInvited":  false,
		}))
	})
})
