# Gabit: A horribly named Golang Habitica API wrapper

This repo is a fork of an api module found in [8Mobius8's go-habits habitica cli](https://github.com/8Mobius8/go-habits). I've been intending to build serveral Habitica oriented tools and bots, but could never find the drive to build an API wrapper from scratch. `8Mobius8` had the best one that I could see, but didn't cover endpoints that I needed (parties and guilds); thus, I decided to expand upon their work.

## Goals
+ Easy to maintain and expand

# LICENSE
Code in the initial commit of this repo matches commit [526f2e4172027e91e886b4af4b480a801bde64a9](https://github.com/8Mobius8/go-habits/commit/526f2e4172027e91e886b4af4b480a801bde64a9) of the parent repo with some minor edits to decouple the project and make it compile. This code is under MIT from `8Mobius8`.

All further edits and expansion are released under MIT by myself.

Basically, it's just MIT.
