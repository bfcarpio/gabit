module gitlab.com/bfcarpio/gabit

// +heroku goVersion go1.16
go 1.16

require (
	github.com/amoghe/distillog v0.0.0-20180726233512-ae382b35b717
	github.com/juju/ratelimit v1.0.1
	github.com/onsi/ginkgo v1.12.0
	github.com/onsi/gomega v1.9.0
)
