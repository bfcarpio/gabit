package gabit

// Group represents a habitica Guild, party, or the Tavern
type Group struct {
	LeaderOnly     *leaderOnly             `json:"leaderOnly,omitempty"`
	Quest          *Quest                  `json:"quest,omitempty"`
	TasksOrder     *tasksOrder             `json:"tasksOrder,omitempty"`
	Purchased      *purchased              `json:"purchased,omitempty"`
	Privacy        *string                 `json:"privacy,omitempty"`
	Chat           []Message               `json:"chat"`
	MemberCount    *int64                  `json:"memberCount,omitempty"`
	ChallengeCount *int64                  `json:"challengeCount,omitempty"`
	Balance        *int64                  `json:"balance,omitempty"`
	ID             *string                 `json:"_id,omitempty"`
	Leader         *leader                 `json:"leader,omitempty"`
	Type           *string                 `json:"type,omitempty"`
	Name           *string                 `json:"name,omitempty"`
	Categories     []category              `json:"categories"`
	Managers       *map[string]interface{} `json:"managers,omitempty"`
	Description    *string                 `json:"description,omitempty"`
	Summary        *string                 `json:"summary,omitempty"`
	GroupID        *string                 `json:"id,omitempty"`
	Order          *string                 `json:"order,omitempty"`
	OrderAscending *string                 `json:"orderAscending,omitempty"`
}

type category struct {
	Slug *string `json:"slug,omitempty"`
	Name *string `json:"name,omitempty"`
}

type leader struct {
	Auth     *string `json:"auth,omitempty"`
	Flags    *string `json:"flags,omitempty"`
	Profile  *string `json:"profile,omitempty"`
	ID       *string `json:"_id,omitempty"`
	LeaderID *string `json:"id,omitempty"`
}

type leaderOnly struct {
	Challenges *bool `json:"challenges,omitempty"`
	GetGems    *bool `json:"getGems,omitempty"`
}

type purchased struct {
	Plan *plan `json:"plan,omitempty"`
}

type plan struct {
	PlanID                *string                  `json:"planId,omitempty"`
	SubscriptionID        *string                  `json:"subscriptionId,omitempty"`
	Owner                 *string                  `json:"owner,omitempty"`
	Quantity              *int64                   `json:"quantity,omitempty"`
	PaymentMethod         *string                  `json:"paymentMethod,omitempty"`
	CustomerID            *string                  `json:"customerId,omitempty"`
	DateCreated           *string                  `json:"dateCreated,omitempty"`
	DateTerminated        *string                  `json:"dateTerminated,omitempty"`
	DateUpdated           *string                  `json:"dateUpdated,omitempty"`
	ExtraMonths           *int64                   `json:"extraMonths,omitempty"`
	GemsBought            *int64                   `json:"gemsBought,omitempty"`
	MysteryItems          []map[string]interface{} `json:"mysteryItems"`
	LastReminderDate      *string                  `json:"lastReminderDate,omitempty"`
	LastBillingDate       *string                  `json:"lastBillingDate,omitempty"`
	AdditionalData        *map[string]interface{}  `json:"additionalData,omitempty"`
	NextPaymentProcessing *string                  `json:"nextPaymentProcessing,omitempty"`
	NextBillingDate       *string                  `json:"nextBillingDate,omitempty"`
	Consecutive           *consecutive             `json:"consecutive,omitempty"`
}

type consecutive struct {
	Count       *int64 `json:"count,omitempty"`
	Offset      *int64 `json:"offset,omitempty"`
	GemCapExtra *int64 `json:"gemCapExtra,omitempty"`
	Trinkets    *int64 `json:"trinkets,omitempty"`
}

// Quest represents a quest that group can be taking
type Quest struct {
	Key        *string                 `json:"key,omitempty"`
	Active     *bool                   `json:"active,omitempty"`
	Leader     *string                 `json:"leader,omitempty"`
	Progress   *Progress               `json:"progress,omitempty"`
	Members    *map[string]interface{} `json:"members,omitempty"`
	Extra      *map[string]interface{} `json:"extra,omitempty"`
	RSVPNeeded *bool                   `json:"RSVPNeeded,omitempty"`
	Completed  interface{}             `json:"completed"`
}

type Progress struct {
	HP             *float64                `json:"hp,omitempty"`
	Collect        *map[string]interface{} `json:"collect,omitempty"`
	Rage           *float64                `json:"rage,omitempty"`
	Up             *int64                  `json:"up,omitempty"`
	Down           *int64                  `json:"down,omitempty"`
	CollectedItems *int64                  `json:"collectedItems,omitempty"`
}

type tasksOrder struct {
	Habits  []string `json:"habits"`
	Dailys  []string `json:"dailys"`
	Todos   []string `json:"todos"`
	Rewards []string `json:"rewards"`
}
