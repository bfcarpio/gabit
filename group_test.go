package gabit_test

import (
	. "github.com/onsi/ginkgo"
	_ "github.com/onsi/ginkgo/extensions/table"
	. "github.com/onsi/gomega"
	"github.com/onsi/gomega/ghttp"
	. "gitlab.com/bfcarpio/gabit"
)

const (
	MysteryTrainGroup = `{ "success": true, "notifications": [], "appVersion": "4.141.1"
    "data": [{
        "leaderOnly": {
            "challenges": false,
            "getGems": false
        },
        "quest": {
            "progress": {
                "collect": {}
            },
            "active": false,
            "members": {},
            "extra": {}
        },
        "tasksOrder": {
            "habits": [],
            "dailys": [],
            "todos": [],
            "rewards": []
        },
        "purchased": {},
        "privacy": "public",
        "chat": [],
        "memberCount": 739,
        "challengeCount": -1,
        "balance": 0,
        "_id": "47fc51a5-0f84-4805-9f2f-e398d628f78e",
        "name": "The Mystery Train",
        "type": "guild",
        "summary": "All Habiticans are welcome on the Mystery Train.  Hop aboard and explore the guilds of Habitica.  Between stops we role-play or talk about recent visits and challenges.",
        "description": "DESCRIPTION",
        "categories": [
            {
                "_id": "5aee5703aeb5d90028ae513b",
                "slug": "hobbies_occupations",
                "name": "hobbies_occupations"
            }
        ],
        "managers": {},
        "leader": {
            "auth": {
                "local": {
                    "username": "Cinnamon_Toast"
                }
            },
            "flags": {
                "verifiedUsername": true
            },
            "profile": {
                "name": "Cinnamon Toast"
            },
            "_id": "8890ddd8-f9d6-4b6d-8491-4d3e18d3ef50",
            "id": "8890ddd8-f9d6-4b6d-8491-4d3e18d3ef50"
        },
        "id": "47fc51a5-0f84-4805-9f2f-e398d628f78e"
    }]
}`
	PostMessageResponse = `{"success": true, "notifications": [],
  "data": {
    "message": {
      "flagCount": 0,
      "flags": {},
      "_id": "76f46449-c723-4bce-af70-013430f28391",
      "id": "76f46449-c723-4bce-af70-013430f28391",
      "text": "My Message",
      "unformattedText": "My Message",
      "info": {},
      "timestamp": "2020-05-09T14:52:10.828Z",
      "likes": {},
      "client": "3rd Party",
      "uuid": "8d8b1c31-85c8-4711-b1c5-20453d9d79b2",
      "contributor": {},
      "backer": {},
      "user": "CassiusVonthill",
      "username": "CassiusVonthill",
      "groupId": "bba93797-2394-4a5b-b528-3d6686661ee7",
      "userStyles": {}
    }
  }}`
)

var _ = Describe("Groups", func() {
	Describe("GetGroups", func() {
		Context("when given 'publicGuilds' type tasks", func() {
			It("calls server for group type tasks for user", func() {
				server.AppendHandlers(
					ghttp.VerifyRequest("GET", "/v3/groups", "type=publicGuilds"),
				)
				habitapi.GetGroups(PublicGuildsType)
			})
		})

	})
	// Describe("GetGroup", func() {
	// 	Context("given a group id", func ()  {
	// 		var group Group
	// 		BeforeEach(func() {
	// 			group = Group{ID: "someID"}
	// 		})
	// 		AfterEach(func() {
	// 			Expect(len(server.ReceivedRequests())).Should(BeNumerically(">", 0))
	// 		})
	// 		Context("and id exists on server", func() {
	// 			BeforeEach(func() {
	// 				server.AppendHandlers(
	// 					ghttp.CombineHandlers(
	// 						ghttp.VerifyRequest("GET", "/v3/groups/"+group.ID),
	// 						ghttp.RespondWith(200, "")
	// 					)
	// 				)
	// 			})
	// 		})
	// 		It("calls server for groups", func ()  {
	// 			server.AppendHandlers(
	// 				ghttp.VerifyRequest("GET", "/v3/groups")
	// 			)
	// 		})
	// 	})
	// }
	Describe("PostMessage", func() {
		Context("given valid message", func() {
			var m Message
			BeforeEach(func() {
				m = Message{}
				m.Text = "My Message"

				server.AppendHandlers(
					ghttp.CombineHandlers(
						ghttp.VerifyRequest("POST", "/v3/groups/party/chat"),
						ghttp.RespondWith(201, PostMessageResponse),
					),
				)
			})
			It("will return a message with a new id", func() {
				message, err := habitapi.PostMessage("party", m.Text)
				Expect(err).ToNot(HaveOccurred())
				Expect(message.ID).ShouldNot(BeEmpty())
			})
			It("will return a message with same text", func() {
				message, err := habitapi.PostMessage("party", m.Text)
				Expect(err).ToNot(HaveOccurred())
				Expect(message.Text).Should(Equal(m.Text))
			})
		})
	})
})
