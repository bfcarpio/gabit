package gabit

// messageResponse is returned inside the `data` field
// when posting a message to a group
type messageResponse struct {
	Message Message `json:"message"`
}

// Message is a message from a group's chat
type Message struct {
	ID              string                 `json:"id"`
	Timestamp       string                 `json:"timestamp,omitempty"`
	Text            string                 `json:"text,omitempty"`
	UnformattedText string                 `json:"unformattedText,omitempty"`
	Info            map[string]interface{} `json:"info,omitempty"`
	User            string                 `json:"user,omitempty"`
	Username        string                 `json:"username,omitempty"`
	Contributor     map[string]interface{} `json:"contributor,omitempty"`
	Backer          map[string]interface{} `json:"backer,omitempty"`
	UUID            string                 `json:"uuid,omitempty"`
	UserStyles      map[string]interface{} `json:"userStyles,omitempty"`
	Flags           map[string]interface{} `json:"flags,omitempty"`
	FlagCount       int64                  `json:"flagCount,omitempty"`
	Likes           map[string]bool        `json:"likes,omitempty"`
	Client          string                 `json:"client,omitempty"`
	Meta            map[string]interface{} `json:"_meta,omitempty"`
	GroupID         string                 `json:"groupId,omitempty"`
	Sent            *bool                  `json:"sent,omitempty"`
	OwnerID         *string                `json:"ownerId,omitempty"`
}
