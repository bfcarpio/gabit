package gabit

import (
	"fmt"
)

// GroupType is a category of groups available
// Represented as an `int` internally and then converted
// to a string for the API.
type GroupType int

// Constant types that should be used when creating or getting
// groups from Habitica server.
const (
	PartyType         GroupType = 0
	GuildType         GroupType = 1
	PrivateGuildsType GroupType = 2
	PublicGuildsType  GroupType = 3
	TavernType        GroupType = 4

	// Utility constant
	Unknown string = "Unknown"
)

func (gt GroupType) String() string {
	groupTypes := []string{
		"party",
		"guilds",
		"privateGuilds",
		"publicGuilds",
		"tavern",
	}
	if gt < PartyType || gt > TavernType {
		return Unknown
	}
	return groupTypes[gt]
}

func (gt GroupType) asURLParam() string {
	return "type=" + gt.String()
}

// GetGroups gets a list of groups of the given GroupType
func (api *HabiticaAPI) GetGroups(gt GroupType) ([]Group, error) {
	groups, err := api.getGroups(gt)
	if err != nil {
		return groups, err
	}
	return groups, nil
}

func (api *HabiticaAPI) getGroups(gt GroupType) ([]Group, error) {
	var groups []Group
	url := "/groups"
	if gt.String() != Unknown {
		url += "?" + gt.asURLParam()
	}
	err := api.Get(url, &groups)
	if err != nil {
		return groups, err
	}
	return groups, nil
}

// GetGroup gets a single group based off it ID
func (api *HabiticaAPI) GetGroup(id string) (Group, error) {
	group, err := api.getGroup(id)
	if err != nil {
		return group, err
	}
	return group, nil
}

func (api *HabiticaAPI) getGroup(id string) (Group, error) {
	var group Group
	url := fmt.Sprintf("/groups/%s", id)
	err := api.Get(url, &group)
	if err != nil {
		return group, err
	}
	return group, nil
}

// GetGroupChat gets the current chat messages available to the group
func (api *HabiticaAPI) GetGroupChat(id string) ([]Message, error) {
	var messages []Message
	url := fmt.Sprintf("/groups/%s/chat", id)
	err := api.Get(url, &messages)
	if err != nil {
		return messages, err
	}

	return messages, nil
}

// PostMessage submits a message to the given group.
// Use it for chatbot-like functionality.
func (api *HabiticaAPI) PostMessage(groupID string, msg string) (Message, error) {
	var mr messageResponse

	// Proper json marshalling means we either
	// need a struct with labels
	// or we just make a quick map
	body := make(map[string]string)
	body["message"] = msg

	url := fmt.Sprintf("/groups/%s/chat", groupID)
	err := api.Post(url, &body, &mr)
	if err != nil {
		return mr.Message, err
	}
	return mr.Message, nil
}
