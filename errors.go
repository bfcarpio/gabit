package gabit

// GabitsError is simple error struct that holds an error code as int
type GabitsError struct {
	msg        string
	StatusCode int
	Path       string
}

func (err *GabitsError) Error() string {
	return err.msg
	//return fmt.Sprintf("Code %d Path %s %s", err.StatusCode, err.Path, err.msg)
}

// NewGabitsError is constructor for GabitsError that includes
// program status code, uri/path, and a simple message string.
func NewGabitsError(message string, code int, path string) *GabitsError {
	return &GabitsError{message, code, path}
}
