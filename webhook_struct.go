package gabit

const (
	TaskActivity      WebhookType = "taskActivity"
	GroupChatReceived WebhookType = "groupChatReceived"
	UserActivity      WebhookType = "userActivity"
	QuestActivity     WebhookType = "questActivity"
)

type WebhookType string

type Webhook struct {
	Enabled bool                   `json:"enabled,omitempty"`
	URL     string                 `json:"url"`
	Label   string                 `json:"label,omitempty"`
	Type    WebhookType            `json:"type,omitempty"`
	Options map[string]interface{} `json:"options,omitempty"`
}
